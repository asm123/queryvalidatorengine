/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asm.indix.queryparser;

import java.util.Scanner;

/**
 *
 * @author asmita
 */
public class Main {
    
    public static void main(String[] args) {
        testWithDefaultInput();
    }
    
    private static void testWithInputFromConsole() {
        Scanner scanner;
        int T;
        String query;
        boolean isValid;
        
        scanner = new Scanner(System.in);
        T = scanner.nextInt();
        
        for (int t = 0; t < T; t++) {
            query = scanner.nextLine();
            
            isValid = new QueryParser().isAValidClause(query);
            System.out.println(isValid);
        }
    }
    
    private static void testWithDefaultInput() {
        boolean isValid;
        String query = "brandId == 23";
        isValid = new QueryParser().isAValidClause(query);
        
        System.out.println("Valid? " + isValid);
        
    }
    
}
