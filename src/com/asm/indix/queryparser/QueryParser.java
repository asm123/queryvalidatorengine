/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asm.indix.queryparser;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author asmita
 */
public class QueryParser {
    
    public QueryParser() {
        
    }
    
    public boolean isAValidClause (String query) {
        query = query.trim();
        if (query.isEmpty()) {
            return false;
        }
        List<String> tokens = Arrays.asList(query.split("\\s+"));
        return isValid(tokens);
    }
    
    private boolean isValid (List<String> tokens) {
        int andIndex, orIndex, inIndex, comparisonIndex, equalsIndex;
        
        // 1. Find AND
        andIndex = tokens.indexOf("AND");
        if (andIndex != -1) {
            return isValidLogicalOperator(tokens, andIndex);
        }
        
        // 2. AND not found, find OR
        
        orIndex = tokens.indexOf("OR");
        if (orIndex != -1) {
            return isValidLogicalOperator(tokens, orIndex);
        }
        
        // 3. Find IN
        inIndex = tokens.indexOf("IN");
        if (inIndex != -1) {
            return isValidIN(tokens, inIndex);
        }
        
        // 4. Find comparison operators
        comparisonIndex = getComparisonOperatorIndex(tokens);
        if (comparisonIndex != -1) {
            return isValidComparison(tokens, comparisonIndex);
        }
        
        // 5. Find equating operators
        equalsIndex = getEqualsOperatorIndex(tokens);
        if (equalsIndex != -1) {
            return isValidEquals(tokens, equalsIndex);
        }
        
        return false;
    }
    
    private boolean isValidLogicalOperator(List<String> tokens, int operatorIndex) {
        if (isOperatorTheLastToken(tokens, operatorIndex)) {
            return false;
        }
        if (operatorIndex == 1) {
            return false;
        }
        if (operatorIndex == tokens.size()-2) {
            return false;
        }
        return isValid(tokens.subList(0, operatorIndex)) 
            && isValid(tokens.subList(operatorIndex+1, tokens.size()));
    }
    
    private boolean isValidIN(List<String> tokens, int inIndex) {
        if (isOperatorTheLastToken(tokens, inIndex)) {
            return false;
        }
        if (inIndex != 1) {
            return false;
        }
        String leftOperand, rightOperandString, valueListString;
        OperandType leftOperandType, rightOperandType;
        int firstIndex, lastIndex;
        String[] valueTokens;
        
        
        leftOperand = tokens.get(0);
        if (!OperandValidator.isValidINAttribute(leftOperand)) {
            return false;
        }
        leftOperandType = OperandValidator.getAttributeType(leftOperand);
        
        rightOperandString = "";
        for (int i = 2; i < tokens.size(); i++) {
            rightOperandString += tokens.get(i).trim();
        }
        
        firstIndex = 0;
        lastIndex = rightOperandString.length()-1;
        if (!(rightOperandString.charAt(firstIndex) == '(' && rightOperandString.charAt(lastIndex) == ')')) {
            return false;
        }
        
        valueListString = rightOperandString.substring(firstIndex+1, lastIndex);
        if (valueListString.isEmpty()) {
            return true;
        }
        valueTokens = valueListString.split(",");
        for (String token: valueTokens) {
            rightOperandType = OperandValidator.getRightOperandType(token.trim());
            if (!OperandValidator.areOperandsCompatible(leftOperandType, rightOperandType)) {
                return false;
            }
        }
        
        return true;
    }
    
    private boolean isValidComparison(List<String> tokens, int comparisonIndex) {
        if (isOperatorTheLastToken(tokens, comparisonIndex)) {
            return false;
        }
        if (tokens.size() != 3) {
            return false;
        }
        if (comparisonIndex != 1) {
            return false;
        }
        String leftOperand, rightOperand, temp;
        OperandType leftOperandType, rightOperandType;
        boolean leftIsAttribute, rightIsAttribute;
        
        leftOperand = tokens.get(0);
        rightOperand = tokens.get(2).trim();
        
        leftIsAttribute = OperandValidator.isAnAttribute(leftOperand);
        rightIsAttribute = OperandValidator.isAnAttribute(rightOperand);
        
        if (!leftIsAttribute && !rightIsAttribute) {
            return false;
        }
        
        if (leftIsAttribute && rightIsAttribute) {
            if (!OperandValidator.isValidComparisonAttribute(leftOperand) 
                    || !OperandValidator.isValidComparisonAttribute(rightOperand)) {
                return false;
            }
            
            leftOperandType = OperandValidator.getAttributeType(leftOperand);
            rightOperandType = OperandValidator.getAttributeType(rightOperand);
        }
        else {
            if (rightIsAttribute) {
                // handling cases where right operand is attribute and left operand is value
                
                temp = leftOperand;
                leftOperand = rightOperand;
                rightOperand = temp;
            }
            leftOperandType = OperandValidator.getAttributeType(leftOperand);
            if (!OperandValidator.isValidComparisonAttribute(leftOperand)) {
                return false;
            }
            
            rightOperandType = OperandValidator.getRightOperandType(rightOperand);
        }
        
        return OperandValidator.areOperandsCompatible(leftOperandType, rightOperandType);
    }
    
    private boolean isValidEquals(List<String> tokens, int equalsIndex) {
        if (tokens.size() != 3) {
            return false;
        }
        if (equalsIndex != 1) {
            return false;
        }
        String leftOperand, rightOperand, temp;
        OperandType leftOperandType, rightOperandType;
        boolean leftIsAttribute, rightIsAttribute;
        
        leftOperand = tokens.get(0);
        rightOperand = tokens.get(2).trim();
        
        leftIsAttribute = OperandValidator.isAnAttribute(leftOperand);
        rightIsAttribute = OperandValidator.isAnAttribute(rightOperand);
        
        if (!leftIsAttribute && !rightIsAttribute) {
            return false;
        }
        
        if (leftIsAttribute && rightIsAttribute) {
            if (!OperandValidator.isValidEqualsAttribute(leftOperand) 
                    || !OperandValidator.isValidEqualsAttribute(rightOperand)) {
                return false;
            }
            
            leftOperandType = OperandValidator.getAttributeType(leftOperand);
            rightOperandType = OperandValidator.getAttributeType(rightOperand);
        }
        else {
            if (rightIsAttribute) {
                // handling cases where right operand is attribute and left operand is value
                
                temp = leftOperand;
                leftOperand = rightOperand;
                rightOperand = temp;
            }
            leftOperandType = OperandValidator.getAttributeType(leftOperand);
            if (!OperandValidator.isValidEqualsAttribute(leftOperand)) {
                return false;
            }
            
            rightOperandType = OperandValidator.getRightOperandType(rightOperand);
        }
        
        return OperandValidator.areOperandsCompatible(leftOperandType, rightOperandType);
    }
    
    private int getComparisonOperatorIndex(List<String> tokens) {
        if (tokens.contains("<=")) {
            return tokens.indexOf("<=");
        }
        if (tokens.contains(">=")) {
            return tokens.indexOf(">=");
        }
        if (tokens.contains("<")) {
            return tokens.indexOf("<");
        }
        if (tokens.contains(">")) {
            return tokens.indexOf(">");
        }
        return -1;
    }
    
    private int getEqualsOperatorIndex(List<String> tokens) {
        if (tokens.contains("==")) {
            return tokens.indexOf("==");
        }
        if (tokens.contains("!=")) {
            return tokens.indexOf("!=");
        }
        return -1;
    }
    
    
    private boolean isOperatorTheLastToken(List<String> tokens, int operatorIndex) {
        return operatorIndex == tokens.size()-1;
    }
    
}
