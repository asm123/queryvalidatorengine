/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asm.indix.queryparser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author asmita
 */
public class OperandValidator {
    private static Map<String, OperandType> attributeTypeMap = new HashMap<>();
    
    private static final Set<String> VALID_IN_ATTRIBUTES = new HashSet<>(Arrays.asList(new String[] 
            {"brandId", "stores", "sellers", "gtins", "mpns"}
        ));
    
    private static final Set<String> VALID_COMPARISON_ATTRIBUTES = new HashSet<>(Arrays.asList(new String[] 
            {"storesCount", "minSalePrice", "maxSalePrice", "updatedAt"}));;
    
    private static final Set<String> VALID_EQUALS_ATTRIBUTES = new HashSet<>(Arrays.asList(new String[] 
            {"brandId", "variantId", "productId"}));;
    
    
    public static boolean isValidINAttribute(String attribute) {
        return VALID_IN_ATTRIBUTES.contains(attribute);
    }
    
    public static boolean isValidComparisonAttribute(String attribute) {
        return VALID_COMPARISON_ATTRIBUTES.contains(attribute);
    }
    
    public static boolean isValidEqualsAttribute(String attribute) {
        return VALID_EQUALS_ATTRIBUTES.contains(attribute);
    }
    
    private static void populateAttributeTypeMap() {
        if (attributeTypeMap.isEmpty()) {
            attributeTypeMap.put("brandId", OperandType.INTEGER);
            attributeTypeMap.put("storesCount", OperandType.INTEGER);
            attributeTypeMap.put("stores", OperandType.INTEGER);
            attributeTypeMap.put("sellers", OperandType.STRING);
            attributeTypeMap.put("gtins", OperandType.STRING);
            attributeTypeMap.put("mpns", OperandType.STRING);
            attributeTypeMap.put("vaiantId", OperandType.STRING);
            attributeTypeMap.put("productId", OperandType.STRING);
            attributeTypeMap.put("minSalePrice", OperandType.DOUBLE);
            attributeTypeMap.put("maxSalePrice", OperandType.DOUBLE);
            attributeTypeMap.put("updatedAt", OperandType.DOUBLE);
        }
    }
    
    public static OperandType getAttributeType(String operand) {
        populateAttributeTypeMap();
        return attributeTypeMap.get(operand);
    } 
    
    public static boolean isAnAttribute(String operand) {
        populateAttributeTypeMap();
        return attributeTypeMap.containsKey(operand);
    }
    
    public static OperandType getRightOperandType(String operand) {
        if (isAnInteger(operand)) {
            return OperandType.INTEGER;
        }
        if (isADouble(operand)) {
            return OperandType.DOUBLE;
        }
        if (isAString(operand)) {
            return OperandType.STRING;
        }
        return null;
    }
    
    public static boolean areOperandsCompatible(OperandType leftOperandType, OperandType rightOperandType) {
        if (leftOperandType == null || rightOperandType == null) {
            return false;
        }
        if (leftOperandType.equals(OperandType.DOUBLE) && rightOperandType.equals(OperandType.INTEGER)) {
            return true;
        }
        return leftOperandType.equals(rightOperandType);
    }
    
    private static boolean isAnInteger(String operand) {
        try {
            Integer.parseInt(operand);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private static boolean isADouble(String operand) {
        try {
            Double.parseDouble(operand);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private static boolean isAString(String operand) {
        int lastIndex = operand.length()-1;
        return operand.charAt(0) == '"' && operand.charAt(lastIndex) == '"';
    }
    
    
    
}
